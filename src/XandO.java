import java.util.Random;
import javax.swing.JOptionPane;

public class XandO {

	public static int start = 0;
	public static char computerSymbol = 'O';
	public static char userSymbol = 'X';
	
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		char[][] a = {  {'.', '.', '.'},
				{'.', '.', '.'},
				{'.', '.', '.'}};
		
		int randomStart = new Random().nextInt(10);
		int i = 1;
		int lastMoveComputer = -1;
		int lastMoveUser = -1;
		int numberOfPlayers = 0;
		boolean playAgain = true;
		
		int difficulty = 2;
		String player1 = "1";
		String player2 = "2";
	
	
	
		while(playAgain) {
			
		computerSymbol = 'O';
		userSymbol = 'X';
		randomStart = new Random().nextInt(10);
		i = 1;
		lastMoveComputer = -1;
		lastMoveUser = -1;	
		
		Frame.showGUI();
		Frame.change(Frame.gameModeGUI());
		
		Thread.sleep(500);		
		
		while(Frame.numberOfPlayers == 0) {
			Thread.sleep(500);
		}	
		
		numberOfPlayers = Frame.numberOfPlayers;
		Frame.numberOfPlayers = 0;
		
		if(numberOfPlayers == 1) {
			
			Frame.change(Frame.chooseDifficultyGUI());
			
			while(Frame.difficulty == -1) {
				Thread.sleep(500);
			}
			
			difficulty = Frame.difficulty;
			Frame.difficulty = -1;
			
			Frame.change(Frame.blankPanel);
			
			player1 = Frame.showName("Player");
			Frame.change(Frame.coinToss());
			
			Thread.sleep(500);
			
			while(Frame.start == -1){
				Thread.sleep(500);
			}
			
			if(randomStart % 2 == Frame.start) {				
				start = 0;	
				Frame.change(Frame.whoStarts("Computer", randomStart % 2));
				Thread.sleep(500);
			}
			else {
				start = 1;
				Frame.change(Frame.whoStarts("Player", randomStart % 2));
				Thread.sleep(500);
			}
			
			Frame.start = -1;
		}
		
		else {
			Frame.change(Frame.blankPanel);
			player1 = Frame.showName("Player 1");
			player2 = Frame.showName("Player 2");
			Frame.change(Frame.pvpStart(player1, player2));
		}
		
		Frame.closeFrame();		
			
			switch(numberOfPlayers) {
			
			case 1: 
			
				switch (difficulty) {
				
				case 2:						
					
				Board.showBoard(a, player1);
				
				if(start == 0){
					computerSymbol = 'X';
					userSymbol = 'O';
					Board.changeBoard(a, "Computer", Board.boardPanel(a));
					Thread.sleep(500);
					lastMoveComputer = randomCorner();
					placeSymbol(a, lastMoveComputer, computerSymbol);					
				}
				
				while(!isOver(a)) {
					Board.changeBoard(a, player1, Board.boardPanel(a));
					
					Thread.sleep(500);
					
					while(Board.lastMove == -1) {
						Thread.sleep(500);
					}
					
					lastMoveUser = Board.lastMove;
					Board.lastMove = -1;
					
					placeSymbol(a, lastMoveUser, userSymbol);
					
					if(isWinner(a, userSymbol)) {
						Board.changeBoard(a, player1, Board.coloredWnnerBoard(a, winningCombinations(a)));
						
						if(Board.win(player1) == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }
						 else 
							playAgain = false;
						
						break;
					}
					
					if(isOver(a))
						break;
					
					Board.changeBoard(a, "Computer", Board.boardPanel(a));
					Thread.sleep(1000);
					
					lastMoveComputer = computerTurnHard(a, lastMoveComputer, lastMoveUser, i);
					placeSymbol(a, lastMoveComputer, computerSymbol);
					
					Board.changeBoard(a, player1, Board.boardPanel(a));
					
					if(isWinner(a, computerSymbol)) {
						
						Board.changeBoard(a, "Computer", Board.coloredWnnerBoard(a, winningCombinations(a)));
						if(Board.win("Computer") == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }
						 else 
							playAgain = false;
						
						break;
					}
					i++;
					
				}
				
				if(isOver(a)) {
					
					Board.changeBoard(a, player1, Board.boardPanel(a));
					
					if(Board.draw() == JOptionPane.YES_OPTION) {
						playAgain = true;
						a = resetTable(a);
					 }
					 else 
						playAgain = false;
					
					break;
				}
				break;
				
				case 1: 
					
					Board.showBoard(a, player1);
					
					if(start == 0){
						computerSymbol = 'X';
						userSymbol = 'O';
						
						Board.changeBoard(a, "Computer", Board.boardPanel(a));
						
						Thread.sleep(500);
						
						lastMoveComputer = randomCorner();
						placeSymbol(a, lastMoveComputer, computerSymbol);	
						
					}
					
					while(!isOver(a)) {
						Board.changeBoard(a, player1, Board.boardPanel(a));
						
						Thread.sleep(500);
						
						while(Board.lastMove == -1) {
							Thread.sleep(500);
						}
						
						lastMoveUser = Board.lastMove;
						Board.lastMove = -1;
						
						placeSymbol(a, lastMoveUser, userSymbol);
						Board.changeBoard(a, "Computer", Board.boardPanel(a));
						
						if(isWinner(a, userSymbol)) {
							Board.changeBoard(a, player1, Board.coloredWnnerBoard(a, winningCombinations(a)));
							
							if(Board.win(player1) == JOptionPane.YES_OPTION) {
								playAgain = true;
								a = resetTable(a);
							 }
							 else 
								playAgain = false;
							
							break;
						}
						
						if(isOver(a))
							break;
						
						lastMoveComputer = computerTurnMedium(a);
						placeSymbol(a, lastMoveComputer, computerSymbol);
						
						Thread.sleep(500);
						
						Board.changeBoard(a, player1, Board.boardPanel(a));
						
						if(isWinner(a, computerSymbol)) {
							Board.changeBoard(a, "Computer", Board.coloredWnnerBoard(a, winningCombinations(a)));
							
							if(Board.win("Comptuer") == JOptionPane.YES_OPTION) {
								playAgain = true;
								a = resetTable(a);
							}
							else 
									playAgain = false;
							
							break;
						}
						i++;
					}
					
					if(isOver(a)) {
					
						Board.changeBoard(a, player1, Board.boardPanel(a));
						
						if(Board.draw() == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }
						 else 
							playAgain = false;
						
						break;
					}
					break;
					
				default:
					
					Board.showBoard(a, player1);
					
					if(start == 0){
						computerSymbol = 'X';
						userSymbol = 'O';
						
						Board.changeBoard(a, "Computer", Board.boardPanel(a));
						
						Thread.sleep(500);
						
						lastMoveComputer = randomCorner();
						placeSymbol(a, lastMoveComputer, computerSymbol);		
						
						Board.changeBoard(a, player1, Board.boardPanel(a));
					}
					
					while(!isOver(a)) {
						Board.changeBoard(a, player1, Board.boardPanel(a));
						
						Thread.sleep(500);
						
						while(Board.lastMove == -1) {
							Thread.sleep(500);
						}
						
						lastMoveUser = Board.lastMove;											
						Board.lastMove = -1;
						
						placeSymbol(a, lastMoveUser, userSymbol);
						Board.changeBoard(a, "Computer", Board.boardPanel(a));
						
						if(isWinner(a, userSymbol)) {							
							Board.changeBoard(a, player1, Board.coloredWnnerBoard(a, winningCombinations(a)));
						
							if(Board.win(player1) == JOptionPane.YES_OPTION) {
								playAgain = true;
								a = resetTable(a);
							 }
							 else 
								playAgain = false;
							
							break;
						}
						
						lastMoveComputer = randomPlace();
						
						while(!isGap(a, lastMoveComputer)) 
							lastMoveComputer = randomPlace();
						
						Thread.sleep(500);
						placeSymbol(a, lastMoveComputer, computerSymbol);
						
						Board.changeBoard(a, player1, Board.boardPanel(a));
						
						if(isWinner(a, computerSymbol)) {							
							Board.changeBoard(a, "Computer", Board.coloredWnnerBoard(a, winningCombinations(a)));
						
							if(Board.win("Comptuer") == JOptionPane.YES_OPTION) {
								playAgain = true;
								a = resetTable(a);
							}
							else 
								playAgain = false;
							
							break;
						}
						i++;
					}
					
					if(isOver(a)) {
						
						Board.changeBoard(a, player1, Board.boardPanel(a));
					
						if(Board.draw() == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }
						 else 
							playAgain = false;
						
						break;
					}
					
					break;
				}
				break;
			
			default: 
				
				Board.showBoard(a, player1);
				
				while(!isOver(a)){
				
					Board.changeBoard(a, player1, Board.boardPanel(a));
					
					Thread.sleep(500);
					
					while(Board.lastMove == -1) {
						Thread.sleep(500);
					}
					
					placeSymbol(a, Board.lastMove, 'X');
					Board.lastMove = -1;
					
					if(isWinner(a, 'X')) {
						Board.changeBoard(a, player1, Board.coloredWnnerBoard(a, winningCombinations(a)));
						
						if(Board.win(player1) == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }
						 else 
							playAgain = false;
						
						break;
					}
					
					Board.changeBoard(a, player2, Board.boardPanel(a));
					
					Thread.sleep(500);
					
					while(Board.lastMove == -1) {
						Thread.sleep(500);
					}
				
					placeSymbol(a, Board.lastMove, 'O');
					Board.lastMove = -1;
					
					if(isWinner(a, 'O')) {
						Board.changeBoard(a, player2, Board.coloredWnnerBoard(a, winningCombinations(a)));
						
						if(Board.win(player2) == JOptionPane.YES_OPTION) {
							playAgain = true;
							a = resetTable(a);
						 }						
						else 
							playAgain = false;
						
						break;
					}
				}
				break;
			}
			
			Board.closeBoard();
		}
	}

	

	
	public static char[][] resetTable(char[][] table) {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				table[i][j] = '.';
			}
		}
		return table;
	}
	

	public static int computerTurnMedium(char[][] table) {
		int position;
		if(start == 0) {
			position = nextMoveWin(table, 'X');
		
			if(position != -1) {			
				return position;
			}
			position = nextMoveWin(table, 'O');
			
			if(position != -1) {
				return position;
			}
		}
		else {
			position = nextMoveWin(table, 'O');
			
			if(position != -1) {			
				return position;
			}
			position = nextMoveWin(table, 'X');
			
			if(position != -1) {
				return position;
			}
		}
		
		position = randomPlace();
		while (!isGap(table, position))
			position = randomPlace();
		return position;
	}

public static int computerTurnHard(char[][] table, int lastMoveComputer, int lastMoveUser, int round) {
		
		int position;
		if(start == 0) {
			position = nextMoveWin(table, 'X');
		
			if(position != -1) {			
				return position;
			}
			position = nextMoveWin(table, 'O');
			
			if(position != -1) {
				return position;
			}
		}
		else {
			position = nextMoveWin(table, 'O');
			
			if(position != -1) {			
				return position;
			}
			position = nextMoveWin(table, 'X');
			
			if(position != -1) {
				return position;
			}
		}
			
		if(isCorner(lastMoveComputer)) {
			if (isCentre(lastMoveUser)) {
				return opositeCorner(lastMoveComputer);
			}
			if(isCorner(lastMoveUser)) {
				if(isGap(table, opositeCorner(lastMoveComputer)))
					return opositeCorner(lastMoveComputer);
				position = randomCorner();
				while(!isGap(table, position)) 
					position = randomCorner();
				return position;	
			}
			if(isMiddle(lastMoveUser)) {
				if(round == 3 || round == 2) {
					if(isGap(table, opositeCorner(lastMoveComputer)))
						return opositeCorner(lastMoveComputer);
					position = randomCorner();
					while(!isGap(table, position)) 
						position = randomCorner();
					
					return position;	
				}
				
				position = gap(lastMoveComputer, lastMoveUser);
				if(isGap(table, position)) {
					
					return position;
				}
			}
		}

		if(isCorner(lastMoveUser)) {
			if(table[1][1] == '.')
				return 11;
			position = randomMiddle();
			while(!isGap(table, position)) 
				position = randomMiddle();
			
			return position;	
		}
		
		if(isMiddle(lastMoveUser)) {
			
			if(table[1][1] == '.')
				return 11;
			
			if(isGap(table, ObetweenX(previousX(table, lastMoveUser), lastMoveUser)))
				return (ObetweenX(previousX(table, lastMoveUser), lastMoveUser));

		}
		
		if(isCentre(lastMoveUser)) {
			position = randomCorner();
			while(!isGap(table, position)) 
				position = randomCorner();
			return position;
		}
		
		position = randomPlace();
		while(!isGap(table, position)) 
			position = randomPlace();
		return position; 
		
	}

	public static boolean isOver(char[][] table) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == '.')
					return false;
			}
		}
		return true;
	}

	public static int previousX(char[][] table, int middle) {

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == userSymbol && i * 10 + j != middle)
					return i * 10 + j;
			}
		}
		return 0;
	}

	public static int randomPlace() {
		Random rn = new Random();
		int rand = rn.nextInt(9);
		switch (rand) {
		case 0:
			return 0;
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 10;
		case 4:
			return 11;
		case 5:
			return 12;
		case 6:
			return 20;
		case 7:
			return 21;

		default:
			return 22;
		}
	}

	public static boolean isGap(char[][] table, int n) {

		return table[n / 10][n % 10] == '.';

	}

	public static int gap(int corner, int middle) {
		switch (corner) {
		case 0:
			switch (middle) {
			case 1:
				return 10;
			case 10:
				return 1;
			case 12:
				return 20;
			case 21:
				return 2;
			}
		case 2:
			switch (middle) {
			case 1:
				return 12;
			case 10:
				return 22;
			case 12:
				return 1;
			case 21:
				return 0;
			}
		case 20:
			switch (middle) {
			case 1:
				return 22;
			case 10:
				return 21;
			case 12:
				return 0;
			case 21:
				return 10;
			}
		default:
			switch (middle) {
			case 1:
				return 20;
			case 10:
				return 2;
			case 12:
				return 21;
			default:
				return 12;
			}

		}
	}

	public static char[][] placeSymbol(char[][] table, int move, char symbol) {
		table[move / 10][move % 10] = symbol;
		return table;
	}

	public static int randomMiddle() {
		Random rn = new Random();
		int middle = rn.nextInt(4);
		switch (middle) {
		case 0:
			return 1;
		case 1:
			return 10;
		case 2:
			return 12;

		default:
			return 21;
		}
	}

	public static int ObetweenX(int corner, int middle) {

		if ((corner == 0 && middle == 21) || (corner == 22 && middle == 10))
			return 20;
		if ((corner == 0 && middle == 12) || (corner == 22 && middle == 1))
			return 2;
		if ((corner == 20 && middle == 12) || (corner == 2 && middle == 21))
			return 22;
		return 0;
	}

	public static int randomCorner() {
		Random rn = new Random();
		int corner = rn.nextInt(4);
		switch (corner) {
		case 0:
			return 0;
		case 1:
			return 2;
		case 2:
			return 20;

		default:
			return 22;
		}
	}

	public static int opositeCorner(int n) {
		return 22 - n;
	}

	public static char[][] userTurn(char[][] table, int move) {

		char[][] newTable = table;
		newTable[move / 10][move % 10] = userSymbol;

		return newTable;

	}

	public static boolean isMiddle(int n) {
		switch (n) {
		case 1:
		case 10:
		case 12:
		case 21:
			return true;

		default:
			return false;
		}
	}

	public static boolean isCentre(int n) {

		return n == 11;

	}

	public static boolean isCorner(int n) {

		switch (n) {
		case 0:
		case 2:
		case 20:
		case 22:
			return true;

		default:
			return false;
		}
	}

	public static boolean isWinner(char[][] table, char c) {

		int i;
		for (i = 0; i < 3; i++) {
			if ((table[i][0] == table[i][1] && table[i][0] == table[i][2] && table[i][0] == c)
					|| (table[0][i] == table[1][i] && table[0][i] == table[2][i] && table[0][i] == c))
				return true;

		}
		if (((table[0][0] == table[1][1] && table[1][1] == table[2][2])
				|| (table[0][2] == table[1][1] && table[1][1] == table[2][0])) && table[1][1] == c)
			return true;

		return false;
	}

	public static int[] winningCombinations(char[][] table) {
		int[] row = new int[3];
		int i;
		for (i = 0; i < 3; i++) {
			if (table[i][0] == table[i][1] && table[i][0] == table[i][2] && table[i][0] != '.') {
				for(int j = 0; j < 3; j++) {
					row[j] = i*10 + j;
				}
				return row;
			}
				
					
			if(table[0][i] == table[1][i] && table[0][i] == table[2][i] && table [0][i] != '.') {
				for(int j = 0; j < 3; j++) {
					row[j] = j*10 + i;
				}
				return row;
			}
				

		}
		if(table[0][0] == table[1][1] && table[1][1] == table[2][2] && table [0][0] != '.') {
			for(int j = 0; j < 3; j++) {
				row[j] = j*10 + j;
			}
			return row;
		}
		
		for(int j = 0; j < 3; j++) {
			row[j] = j*10 + 2 - j;
		}
		
		return row;
		
		
	}
	
	public static int nextMoveWin(char[][] table, char c) {
		int i;
		for (i = 0; i < 3; i++) {
			if (checkWin(table[i][0], table[i][1], table[i][2], c))
				return i * 10 + positionWin(table[i][0], table[i][1], table[i][2]);

			if (checkWin(table[0][i], table[1][i], table[2][i], c))
				return positionWin(table[0][i], table[1][i], table[2][i]) * 10 + i;

		}
		if (checkWin(table[0][0], table[1][1], table[2][2], c))
			return positionWin(table[0][0], table[1][1], table[2][2]) * 11;
		if (checkWin(table[0][2], table[1][1], table[2][0], c))
			return positionWin(table[0][2], table[1][1], table[2][0]) * 10 + 2
					- positionWin(table[0][2], table[1][1], table[2][0]);

		return -1;
	}

	public static int positionWin(char a, char b, char c) {

		if (a == '.')
			return 0;
		if (b == '.')
			return 1;
		return 2;

	}

	public static boolean checkWin(char a, char b, char c, char ch) {
		int xOccur = 0;
		int oOccur = 0;
		int dotOccur = 0;
		char[] symbols = { a, b, c };
		for (int i = 0; i < 3; i++) {
			switch (symbols[i]) {
			case 'X':
				xOccur++;
				break;
			case 'O':
				oOccur++;
				break;
			default:
				dotOccur++;
				break;
			}
		}

		if (dotOccur == 1) {
			if (xOccur == 2) {
				if (ch == userSymbol)
					return true;
				return false;
			} else if (oOccur == 2) {
				if (ch == computerSymbol)
					return true;
				return false;
			}

		}
		return false;
	}

}
