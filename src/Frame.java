import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Frame {

	
	
	static int start = -1;
	static int numberOfPlayers = 0;
	static int difficulty = -1;
	
	static JFrame mainFrame = new JFrame("Tic tac toe game");
	
	static JTextField nameField = new JTextField(10);
	
	static JPanel nameMode = new JPanel(new BorderLayout());
	static JPanel gameMode = new JPanel();
	static JPanel chooseDifficulty = new JPanel();
	static JPanel blankPanel = new JPanel();
	
	static JButton covp = new JButton("Computer vs User");
	static JButton pvp = new JButton("Player vs player");
	static JButton easyMode = new JButton("Easy");
	static JButton mediumMode = new JButton("Medium");
	static JButton hardMode = new JButton("Hard");
	static JButton headsButton = new JButton("Heads");
	static JButton tailButton = new JButton("Tails");
	
	public static JPanel gameModeGUI() {
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gameMode.setLayout(new GridBagLayout());
		
		covp.setPreferredSize(new Dimension(300, 100));
		pvp.setPreferredSize(new Dimension(300, 100));
		
		gameMode.add(covp, gbc);
		gameMode.add(pvp, gbc);
	
		covp.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == covp) 
		      
		    	   numberOfPlayers = 1;
		      
		       
		      }
		 });
		
		pvp.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == pvp) 
		       
		    	   numberOfPlayers = 2;
		       		
		       	
		      }
		 });
		 return gameMode;
	}
	
	public static JPanel chooseDifficultyGUI() {
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		easyMode.setPreferredSize(new Dimension(300, 100));
		mediumMode.setPreferredSize(new Dimension(300, 100));
		hardMode.setPreferredSize(new Dimension(300, 100));
		
		chooseDifficulty.setLayout(new GridBagLayout());
		chooseDifficulty.add(easyMode, gbc);
		chooseDifficulty.add(mediumMode, gbc);
		chooseDifficulty.add(hardMode, gbc);
		
		easyMode.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == easyMode) 
		      
		    	   difficulty = 0;
		     
		       
		      }
		 });
		mediumMode.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == mediumMode) 
		      
		    	   difficulty = 1;
		       
		       
		      }
		 });
		hardMode.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == hardMode) 

		    	   difficulty = 2;
		   
		       
		      }
		 });
		return chooseDifficulty;
		
	}
	

	
	public static void showGUI() {
		
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(600, 500);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
        
	}
	public static void change(JPanel actualPanel) {

		mainFrame.getContentPane().removeAll();
		mainFrame.getContentPane().invalidate();		
		mainFrame.getContentPane().add(actualPanel);
		mainFrame.getContentPane().revalidate();
		mainFrame.getContentPane().repaint();
	}
	
	public static String showName(String name) {
		new JOptionPane();
		String playerName = JOptionPane.showInputDialog(name + " name : " );
		
		return playerName;
	}
	
	public static JPanel pvpStart(String player1, String player2) {
		
		JPanel pvpStartPanel = new JPanel();
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		pvpStartPanel.setLayout(new GridBagLayout());
		int start = new Random().nextInt(10);
		
		if(start % 2 == 0) {
			pvpStartPanel.add(new JLabel(player1 + " starts"), gbc);
		}
		else 
			pvpStartPanel.add(new JLabel(player2 + " starts"), gbc);
		
		return pvpStartPanel;
	}
	
	public static JPanel whoStarts (String start, int number) {
		JPanel selectStart = new JPanel();
		
		
		selectStart.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		tailButton.setPreferredSize(new Dimension(300, 100));
		headsButton.setPreferredSize(new Dimension(300, 100));
		
		selectStart.add(tailButton, gbc);
		selectStart.add(headsButton, gbc);
		if(number == 0) 
			selectStart.add(new JLabel("TAILS"), gbc); 
		else 
			selectStart.add(new JLabel("HEADS"), gbc);
		
		selectStart.add(new JLabel(start + " starts first"), gbc);
		
		return selectStart;
		
	}
	
	public static JPanel coinToss() {
		JPanel selectStart = new JPanel();
		
		selectStart.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		tailButton.setPreferredSize(new Dimension(300, 100));
		headsButton.setPreferredSize(new Dimension(300, 100));
		
		selectStart.add(tailButton, gbc);
		selectStart.add(headsButton, gbc);
		tailButton.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == tailButton) 		       
		    	   start = 1;		       				       	
		      }
		 });
		headsButton.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {
		       if(e.getSource() == headsButton) 		       
		    	   start = 0;       				       	
		      }
		 });
		return selectStart;
	}
	
	
	public static void closeFrame() {		
		mainFrame.dispose();
	}

	
}
