import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Board {

	static JFrame mainFrame = new JFrame("Tic");
	
	
	static int lastMove = -1;
	

	public static void showBoard (char[][]table, String name) {
		
		JLabel nameLabel = new JLabel(name + "'s turn");
		
		nameLabel.setHorizontalAlignment(SwingConstants.CENTER);	
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(600, 500);
		mainFrame.setVisible(true);
		mainFrame.add(nameLabel, BorderLayout.NORTH);
		mainFrame.add(boardPanel(table));
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}
	
	public static void closeBoard() {
		mainFrame.dispose();
		
	}
	
	public static void changeBoard (char[][] table, String name, JPanel acutalPanel) {
		
		JLabel nameLabel = new JLabel(name + "'s turn");	
		
		nameLabel.setHorizontalAlignment(SwingConstants.CENTER);				
		mainFrame.getContentPane().removeAll();
		mainFrame.getContentPane().invalidate();
		mainFrame.getContentPane().add(nameLabel, BorderLayout.NORTH);
		mainFrame.getContentPane().add(acutalPanel);
		mainFrame.getContentPane().revalidate();
		mainFrame.getContentPane().repaint();
	}
	
	public static int win (String name) {
		int n = JOptionPane.showConfirmDialog(
	            null,
	            name + " win\n" +
	            "Play again?",
	            "Tic Tac Toe",
	            JOptionPane.YES_NO_OPTION);
		return n;
	}

	public static int draw () {
		int n = JOptionPane.showConfirmDialog(
				null,
				"Draw \n" + 
				"Play again?",
				"Tic Tac Toe",
				JOptionPane.YES_NO_OPTION);
		return n;
	}
	
	public static JPanel coloredWnnerBoard(char[][] table, int[] positions) {
		JPanel panel = new JPanel(new GridLayout(3, 3));
		JButton b1 = new JButton("" + table[0][0]);
		JButton b2 = new JButton("" + table[0][1]);
		JButton b3 = new JButton("" + table[0][2]);
		JButton b4 = new JButton("" + table[1][0]);
		JButton b5 = new JButton("" + table[1][1]);
		JButton b6 = new JButton("" + table[1][2]);
		JButton b7 = new JButton("" + table[2][0]);
		JButton b8 = new JButton("" + table[2][1]);
		JButton b9 = new JButton("" + table[2][2]);
		
		b1.setEnabled(false);
		b2.setEnabled(false);
		b3.setEnabled(false);
		b4.setEnabled(false);
		b5.setEnabled(false);
		b6.setEnabled(false);
		b7.setEnabled(false);
		b8.setEnabled(false);
		b9.setEnabled(false);
		
		if(valueInArray(positions, 0))
			b1.setBackground(Color.RED);
		if(valueInArray(positions, 1))
			b2.setBackground(Color.RED);
		if(valueInArray(positions, 2))
			b3.setBackground(Color.RED);
		if(valueInArray(positions, 10))
			b4.setBackground(Color.RED);
		if(valueInArray(positions, 11))
			b5.setBackground(Color.RED);
		if(valueInArray(positions, 12))
			b6.setBackground(Color.RED);
		if(valueInArray(positions, 20))
			b7.setBackground(Color.RED);
		if(valueInArray(positions, 21))
			b8.setBackground(Color.RED);
		if(valueInArray(positions, 22))
			b9.setBackground(Color.RED);
		
		panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		panel.add(b4);
		panel.add(b5);
		panel.add(b6);
		panel.add(b7);
		panel.add(b8);
		panel.add(b9);
		
		return panel;
		
	}
	
	public static boolean valueInArray (int[] arr, int value) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == value)
				return true;
		}
		return false;
	}
	
	public static JPanel boardPanel (char[][] table) {
		JPanel panel = new JPanel(new GridLayout(3, 3));
		JButton b1 = new JButton("" + table[0][0]);
		JButton b2 = new JButton("" + table[0][1]);
		JButton b3 = new JButton("" + table[0][2]);
		JButton b4 = new JButton("" + table[1][0]);
		JButton b5 = new JButton("" + table[1][1]);
		JButton b6 = new JButton("" + table[1][2]);
		JButton b7 = new JButton("" + table[2][0]);
		JButton b8 = new JButton("" + table[2][1]);
		JButton b9 = new JButton("" + table[2][2]);
		
		if(table[0][0] != '.')
			b1.setEnabled(false);
		
		b1.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b1) 
		    	   lastMove = 0;   		       
		      }
		 });
		
		if(table[0][1] != '.')
			b2.setEnabled(false);
		
		b2.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b2) 
		    	   lastMove = 1;   		       
		      }
		 });
		
		if(table[0][2] != '.')
			b3.setEnabled(false);
		
		b3.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b3) 
		    	   lastMove = 2;   		       
		      }
		 });
		
		if(table[1][0] != '.')
			b4.setEnabled(false);
		
		b4.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b4) 
		    	   lastMove = 10;   		       
		      }
		 });
		
		if(table[1][1] != '.')
			b5.setEnabled(false);
		
		b5.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b5) 
		    	   lastMove = 11;   		       
		      }
		 });
		
		if(table[1][2] != '.')
			b6.setEnabled(false);
		
		b6.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b6) 
		    	   lastMove = 12;   		       
		      }
		 });
		
		if(table[2][0] != '.')
			b7.setEnabled(false);
		
		b7.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b7) 
		    	   lastMove = 20;   		       
		      }
		 });
		
		if(table[2][1] != '.')
			b8.setEnabled(false);
		
		b8.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b8) 
		    	   lastMove = 21;   		       
		      }
		 });
		
		if(table[2][2] != '.')
			b9.setEnabled(false);
		
		b9.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) 
		      {		      
				 if(e.getSource() == b9) 
		    	   lastMove = 22;   		       
		      }
		 });
		
		
		panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		panel.add(b4);
		panel.add(b5);
		panel.add(b6);
		panel.add(b7);
		panel.add(b8);
		panel.add(b9);
		
		return panel;
		
	}
	
}

